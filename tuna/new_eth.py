# Copyright (C) 2022 John Kacur
# SPDX-License-Identifier: GPL-2.0-only
""" A few functions similar to ethtool """
import os
import socket

def get_active_devices():
    """ return a list of network devices """
    ret = []

    for device in socket.if_nameindex():
        ret.append(device[1])

    return ret

def get_module(intf):
    """ return the kernel module for the given network interface """
    if intf == 'lo':
        return ""
    myp = f'/sys/class/net/{intf}/device/driver'
    if os.path.exists(myp):
        return os.path.basename(os.readlink(myp))
    if os.path.exists(f'/sys/class/net/{intf}/bridge'):
        return 'bridge'
    if os.path.exists(f'/sys/class/net/{intf}/tun_flags'):
        return 'tun'
    return ""

if __name__ == "__main__":
    nics = get_active_devices()
    print(f'nics = {nics}')

    for intf in nics:
        driver = get_module(intf)
        if driver:
            print(f'{intf}, {driver}')
        else:
            print(f'{intf}')
