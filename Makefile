#
# SPDX-License-Identifier: GPL-2.0-only
#

.PHONY: pyclean
pyclean:
	@find . -type f \( -name \*~ -o -name \*.pyc \) -delete

.PHONY: tags
tags:
	ctags -R --extras=+fq --python-kinds=+cfmvi

.PHONY: cleantags
cleantags:
	rm -f tags

.PHONY: cleanlogs
cleanlogs:
	rm -rf tuna-20*

.PHONY: clean
clean: pyclean
